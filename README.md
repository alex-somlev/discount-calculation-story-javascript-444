# Discount Calculation Story

**To read the story**: https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fdiscount-calculation-story-javascript

**To code yourself**: https://git.epam.com/Refactoring-Stories/discount-calculation-story-javascript

**Estimated reading time**: 45 minutes

## Story Outline
Read a small story about calculation of a discount during the online purchase.

This story is about violations of clean design principles with focus on
Dependency Inversion Principle (D in SOLID).

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_design, #javascript, #dependency_inversion_principle
