'use strict';

const Product = require('./product');

class Order {
  constructor(data = {}) {
    this.id = data.id;
    this.orderPrice = data.orderPrice;
    this.products = this.setProducts(data.products);
    this.purchaseDate = data.purchaseDate;
    this.buyerId = data.buyerId;
    this.discount = data.discount;
    this.totalTax = data.totalTax;
  }

  getId() {
    return this.id;
  }

  setId(id) {
    this.id = id;
  }

  getOrderPrice() {
    return this.orderPrice;
  }

  setOrderPrice(orderPrice) {
    this.orderPrice = orderPrice;
  }

  getProducts() {
    return this.products;
  }

  setProducts(products) {
    this.products = products.map(product => new Product(product));
  }

  getPurchaseDate() {
    return this.purchaseDate;
  }

  setPurchaseDate(purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  getBuyerId() {
    return this.buyerId;
  }

  setBuyerId(buyerId) {
    this.buyerId = buyerId;
  }

  getDiscount() {
    return this.discount;
  }

  setDiscount(discount) {
    this.discount = discount;
  }

  getTotalTax() {
    return this.totalTax;
  }

  setTotalTax(totalTax) {
    this.totalTax = totalTax;
  }
}

module.exports = Order;