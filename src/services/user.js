'use strict';

const FIVE_PERCENT_DISCOUNT = 0.05;
const TEN_PERCENT_DISCOUNT = 0.1;
const FIFTEEN_PERCENT_DISCOUNT = 0.15;
const DISCOUNT_LEVEL1_THRESHOLD_USD = 1000;
const DISCOUNT_LEVEL2_THRESHOLD_USD = 3000;
const DISCOUNT_LEVEL3_THRESHOLD_USD = 5000;


class UserService {
  constructor() {
    this.discountThresholdPercentageMap = {
      [FIVE_PERCENT_DISCOUNT]: DISCOUNT_LEVEL1_THRESHOLD_USD,
      [TEN_PERCENT_DISCOUNT]: DISCOUNT_LEVEL2_THRESHOLD_USD,
      [FIFTEEN_PERCENT_DISCOUNT]: DISCOUNT_LEVEL3_THRESHOLD_USD
    }
  }

  calculateDiscountForUser(userModel) {
    const totalPriceOfAllOrders = this.calculateTotalPriceOfAllOrders(userModel);
    for (const key of Object.keys(this.discountThresholdPercentageMap).reverse()) {
      if (totalPriceOfAllOrders >= this.discountThresholdPercentageMap[key]) {
        return Number(key);
      }
    }
    return 0;
  }

  calculateTotalPriceOfAllOrders(userModel) {
    return userModel.getOrders()
      .map(order => order.getOrderPrice() + order.getTotalTax())
      .reduce((totalPrice, orderPrice) => totalPrice + orderPrice, 0)
  }
}

module.exports = UserService;